# (Unofficial) ANU-coloured LaTeX Beamer template

If you use the LaTeX beamer package to typeset your lecture slides,
then you might find this template helpful.

This isn't a standalone beamer theme, it's just a template `.tex` file
which uses the [metropolis beamer theme](https://github.com/bardsoftware/template-beamer-metropolis)
plus a couple of colour tweaks. This is not officially sanctioned by 
anyone, although pull requests from the marketing department are welcome.

# Demo/example

I know you're dying to see an example---[here you go](template.pdf).

# Usage

Assuming you've got all the required packages, just start with the
[template](template.tex) and hack away. There are a few example slides
in there for things you might want to do.

## Source code highlighting with pygments and minted

If you want to include source code listings in your slides, you use
the python [pygments](http://pygments.org/) library (through the LaTeX
[minted](https://github.com/gpoore/minted) package) to give you pretty
syntax colours for your listings. This is a bit fiddly to set up, see
the example slide toward the end of the [template](template.tex).

# License

The MIT License (MIT)

Copyright (c) 2016 Ben Swift

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
